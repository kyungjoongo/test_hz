import React, { useEffect, useState } from 'react';
import 'scss/components/gnb.scss';
import menuService from '../services/MenuService';
import { ActivityIndicator, Text, TouchableOpacity, View } from 'react-native-web';
import workService from '../services/WorkService';

export default function Gnb() {

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        init();
    }, []);

    async function init() {
        setLoading(true);
        await menuService.getGnbMenuList();
        setLoading(false);
    }

    if (loading) {
        return (
            <View>
                <ActivityIndicator size={'large'}/>
            </View>
        );
    }

    return (
        <div id="gnb">
            <View>
                {menuService.gnbList.map((item, index) => {
                    return (
                        <View
                            key={index.toString()}
                            style={{
                                marginVertical: 10,
                                marginLeft: 3
                            }}
                        >
                            <Text style={{
                                fontSize: 20,
                                fontWeight: 'bold'
                            }}>{item.name}
                            </Text>
                            {item.children.map((innerItem, jIndex) => {
                                return (
                                    <View
                                        key={jIndex.toString()}
                                        style={{
                                            marginLeft: 25,
                                            marginTop: 10
                                        }}


                                    >
                                        <View>
                                            <Text style={{
                                                fontSize: 17,
                                                color: 'black'
                                            }}>{innerItem.name}</Text>
                                        </View>
                                        {innerItem.children?.map((secondInnerItem, zIndex) => {
                                            return (
                                                <View
                                                    key={zIndex.toString()}
                                                    style={{
                                                        marginTop: 5,
                                                        marginLeft: 30

                                                    }}
                                                >
                                                    <TouchableOpacity

                                                        onPress={() => {
                                                            workService.currentPathName = secondInnerItem.pathname;
                                                        }}
                                                    >
                                                        <Text
                                                            style={{
                                                                color: 'navy',
                                                                fontSize: 15
                                                            }}
                                                        >
                                                            {secondInnerItem.name}
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            );
                                        })}
                                    </View>
                                );
                            })}
                        </View>
                    );
                })}
            </View>
        </div>
    );
}
