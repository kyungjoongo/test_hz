import { makeAutoObservable } from 'mobx';
import React from 'react';

class WorkService {
    constructor() {
        makeAutoObservable(this);
    }

    numOfColumns = 1;

    setNumOfColumns(value) {
        const boxMargin = 0.5;
        this.numOfColumns = value;
        if (value === 1) {
            this.columnPercentage = 100;
        } else if (value === 2) {
            this.columnPercentage = 100 / 2 - boxMargin;
        } else if (value === 3) {
            this.columnPercentage = 100 / 3 - boxMargin;
        } else if (value === 4) {
            this.columnPercentage = 100 / 4 - boxMargin;
        }
    }

    columnPercentage = 100;

    items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    currentPathName = undefined;
}

const workService = new WorkService();

export default workService;
