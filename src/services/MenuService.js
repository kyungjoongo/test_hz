import { makeAutoObservable } from 'mobx';
import axios from 'axios';
import React from 'react';

class MenuService {
    constructor() {
        makeAutoObservable(this);
    }

    gnbList = [];

    async getGnbMenuList() {
        try {
            let _response = await axios.get('http://haezoom-frontend-homework.s3.ap-northeast-2.amazonaws.com/response.json');
            console.log('temp=====>', _response.data);
            this.gnbList = _response.data;
        } catch (e) {
            alert(e.toString());
        }
    }

}

const menuService = new MenuService();

export default menuService;
