import React from 'react';
import 'scss/pages/work.scss';
import { Observer } from 'mobx-react-lite';
import workService from '../services/WorkService';
import { View } from 'react-native-web';

export default function Work() {

    return (
        <Observer>
            {() => (
                <div className="area-work">
                    <div className="page-top">
                        <h3 className="pathname">pathName: {workService.currentPathName}</h3>
                        <div className="button-wrap">
                            <button
                                style={{
                                    background: workService.numOfColumns === 1 ? 'orange' : null,
                                    color: workService.numOfColumns === 1 ? 'white' : 'black',
                                    fontWeight: 'bold'
                                }}
                                onClick={() => {
                                    workService.setNumOfColumns(1);
                                }}
                            > 1
                            </button>
                            <button
                                style={{
                                    background: workService.numOfColumns === 2 ? 'orange' : null,
                                    color: workService.numOfColumns === 2 ? 'white' : 'black',
                                    fontWeight: 'bold'
                                }}
                                onClick={() => {
                                    workService.setNumOfColumns(2);
                                }}
                            > 2
                            </button>
                            <button
                                style={{
                                    background: workService.numOfColumns === 3 ? 'orange' : null,
                                    color: workService.numOfColumns === 3 ? 'white' : 'black',
                                    fontWeight: 'bold'
                                }}
                                onClick={() => {
                                    workService.setNumOfColumns(3);
                                }}
                            > 3
                            </button>
                            <button
                                style={{
                                    background: workService.numOfColumns === 4 ? 'orange' : null,
                                    color: workService.numOfColumns === 4 ? 'white' : 'black',
                                    fontWeight: 'bold'
                                }}
                                onClick={() => {
                                    workService.setNumOfColumns(4);
                                }}
                            > 4
                            </button>
                        </div>
                    </div>
                    <div>
                        numOfColumns: {workService.numOfColumns}
                    </div>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            alignItems: 'center',
                            width: '100%'

                        }}

                    >
                        {workService.items.map((item, idx) => {
                            return (
                                <View
                                    key={idx.toString()}
                                    style={{
                                        width: workService.columnPercentage + '%',
                                        margin: 0.5,
                                        backgroundColor: 'orange',
                                        padding: 0.5,
                                        height: 120,
                                        alignSelf: 'center',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <p style={{
                                        fontSize: 40,
                                        textAlign: 'center',
                                        verticalAlign: 'center',
                                        color: 'white'
                                    }}>{item.toString()}</p>
                                </View>
                            );
                        })}
                    </View>
                </div>

            )}
        </Observer>
    );
}
